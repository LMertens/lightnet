SHELL := /bin/bash
.ONESHELL:
.PHONY: docs doctest unittest lint test build venv
.SILENT: docs doctest unittest lint build venv
.NOTPARALLEL: docs doctest unittest lint test build venv

######################

docs: notebook:= 0
docs:
	[ -s .venv/bin/activate ] && source .venv/bin/activate
	cd ./docs && JNB=${notebook} make clean html

######################

doctest:
	[ -s .venv/bin/activate ] && source .venv/bin/activate
	cd docs && make doctest

######################

unittest: file := ./test/
unittest: expr :=
unittest: marker :=
unittest: flags :=
unittest: EXPR := $(if $(strip ${expr}), -k "${expr}",)
unittest: MARKER := $(if $(strip ${marker}), -m "${marker}",)
unittest:
	[ -s .venv/bin/activate ] && source .venv/bin/activate
	BB_LOGLVL=warning python -m pytest ${flags} ${EXPR} ${MARKER} ${file}

######################

lint: args :=
lint:
	[ -s .venv/bin/activate ] && source .venv/bin/activate
	flake8 ${args} lightnet/
	flake8 ${args} test/

######################

test: unittest doctest lint

######################

build: test
	[ -s .venv/bin/activate ] && source .venv/bin/activate
	rm -rf dist/*
	python setup.py sdist bdist_wheel
	for w in dist/*.whl; do auditwheel repair $$w; done
	echo -e '\n \033[1mUpload your package to (test) PyPi\e[0m'

######################

venv:
	python3.8 -m venv .venv
	source .venv/bin/activate
	pip install -U pip
	pip install opencv-python pillow
	pip install torch==1.9.1+cu111 torchvision==0.10.1+cu111 -f https://download.pytorch.org/whl/torch_stable.html
	pip install -r develop.txt
