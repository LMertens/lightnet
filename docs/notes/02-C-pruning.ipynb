{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-warning\">\n",
    "\n",
    "**Error:**\n",
    "\n",
    "Pruning seems to be broken in the newest PyTorch version!\n",
    "    \n",
    "In order to prune a model, we first convert it to ONNX and use that representation to get a dependency map between the different operations.  \n",
    "The conversion from PyTorch->ONNX got improved in the latest releases, meaning a lot of operations that were defined as ATen are now proper ONNX operations.\n",
    "However, our code still searches for these old ATen operations and will thus find no (prunable) convolutions at all.  \n",
    "If we manage to fix the dependency map, everything should work correctly.\n",
    "    \n",
    "We can fix this by looking for the proper operations in the ONNX model, but this might not be the most stable method.\n",
    "It might be better to generate the dependency map from the TorchScript models, as I except naming to be more consistent there...\n",
    "    \n",
    "Anyway, if you feel up for the challenge of fixing the pruning API, feel free to start a PR / issue to discuss things in depth!\n",
    "    \n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Pruning\n",
    "Models are usually designed to work well on a specific (academic) dataset.\n",
    "This is not different for object detection and as such, most models are designed around the Pascal VOC or COCO dataset.\n",
    "However, operational use cases might contain very different data, and usually this data is also simpler or more coherent than these academic datasets.\n",
    "\n",
    "Whilst adapting existing networks on a case-by-case basis is certainly possible, it is quite a tiresome and daunting work.\n",
    "An easier technique is to use existing networks, train them on your data and call it a day!  \n",
    "Nevertheless, one might wonder whether the chosen network is optimal for their situation or whether the model might be computationally more expensive than necessary...  \n",
    "Meet **pruning**, which is a technique that will reduce the number of computations in a network in an automated manner, by looking at the importance of the weights in the model.  \n",
    "\n",
    "Lightnet implements channel-wise soft and hard pruning of convolutions, which means we either set the weights of a certain channel in a convolution to zero (soft), or completely strip the channel from the convolution (hard), which results in less computations and smaller models.\n",
    "In this tutorial, we will take a look at how you can use the pruning functionality in lightnet, in order to automatically reduce the number of computations of your model, without losing accuracy.\n",
    "\n",
    "<div class=\"alert alert-info\">\n",
    "\n",
    "**Warning:**\n",
    "\n",
    "The pruning functionality in lightnet requires the [onnx](https://github.com/onnx/onnx) library.  \n",
    "You can install it by running `pip install onnx`.\n",
    "\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "ExecuteTime": {
     "end_time": "2020-12-14T11:27:40.923631Z",
     "start_time": "2020-12-14T11:27:39.164971Z"
    }
   },
   "outputs": [],
   "source": [
    "# Basic imports\n",
    "import lightnet as ln\n",
    "import torch\n",
    "import torchvision\n",
    "import numpy as np\n",
    "from PIL import Image\n",
    "import matplotlib.pyplot as plt\n",
    "import brambox as bb\n",
    "import os\n",
    "import warnings\n",
    "\n",
    "# Settings\n",
    "ln.logger.setConsoleLevel('ERROR')             # Only show error log messages\n",
    "bb.logger.setConsoleLevel('ERROR')             # Only show error log messages\n",
    "\n",
    "# This is only to have a cleaner documentation and should generally not be used\n",
    "warnings.filterwarnings('ignore')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Soft Pruning\n",
    "\n",
    "We will first look at soft pruning, which means we set the weights of a certain channel of a convolution to zero.  \n",
    "This can be used before hard pruning, by iteratively soft pruning a certain percentage of a network and retraining until the original accuracy is reached.\n",
    "Once the network does not need retraining to reach the original accuracy, we know that this percentage of the network is not necessary and we can remove it.  \n",
    "Another use case for soft-pruning is that it is a kind of regularisation technique, which can be used similar to dropout, in order to increase the accuracy of a network during training.\n",
    "\n",
    "Before pruning a network, you start by training it, which we assume you already did.\n",
    "We thus load our network and define some extra bits and bops which we will need.\n",
    "Once we have our model and optimizer, we can create a [Pruner](../api/generated/lightnet.prune.Pruner.rst).\n",
    "Lightnet comes with a few different pruner implementations, here we will use a basic [L2Pruner](../api/generated/lightnet.prune.L2Pruner.rst).\n",
    "Once we build our pruner, we can look at the ``prunable_channels`` property, which shows how many convolutional channels can potentially be pruned.\n",
    "\n",
    "<div class=\"alert alert-info\">\n",
    "\n",
    "**Note:**\n",
    "\n",
    "The ``prunable_channels`` property returns the total amount of channels in the prunable convolutions.\n",
    "Note that we never prune the last channel of a convolution and thus cannot prune all of these channels.\n",
    "\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "ExecuteTime": {
     "end_time": "2020-12-14T11:27:45.768353Z",
     "start_time": "2020-12-14T11:27:40.925708Z"
    },
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "# Network\n",
    "net = ln.models.YoloV2(20)\n",
    "net.load('./yolov2-voc.pt')\n",
    "dimensions = (1, 3, 416, 416)\n",
    "\n",
    "# Pruner\n",
    "pruner = ln.prune.L2Pruner(net, input_dimensions=dimensions, manner=\"soft\")\n",
    "pruner.prunable_channels"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As we can see, the puner shows there is a total amount of 9248 prunable channels in this network.  \n",
    "In order to prune channels, we simply [\\_\\_call\\_\\_](../api/generated/lightnet.prune.Pruner.rst) the pruner with a percentage.\n",
    "\n",
    "This function will return the actual number of pruned channels, which is 924 in this particular case."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "ExecuteTime": {
     "end_time": "2020-12-14T11:27:45.819062Z",
     "start_time": "2020-12-14T11:27:45.770231Z"
    }
   },
   "outputs": [],
   "source": [
    "# Prune 10% of the network\n",
    "pruner(0.10)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "That was it for soft pruning.\n",
    "Simple, right!  \n",
    "Now, let's take a look at hard pruning."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Hard Pruning\n",
    "\n",
    "Hard pruning is not so different from soft pruning with Lightnet, but you need to understand that hard pruning will effectively modify your network architecture.\n",
    "This has a few consequences for the rest of your pipelines.\n",
    "\n",
    "Any object which holds a reference to your network parameters, will need to be updated each time you prune your network.\n",
    "When (re-)training a network, usually this means your optimizer.  \n",
    "A second consequence is that you will not be able to simply load your model anymore, as the model does not keep track of which channels were pruned.\n",
    "\n",
    "Let's take a look at the optimizer issue first!  \n",
    "The solution is quite simple, you recreate a new optimizer each time you prune your network.\n",
    "However, this is quite tedious to do and thus, you can pass your optimizer to the pruner, and the pruner will automatically adapt your optimizer for you."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "ExecuteTime": {
     "end_time": "2020-12-14T11:27:50.411685Z",
     "start_time": "2020-12-14T11:27:45.821557Z"
    }
   },
   "outputs": [],
   "source": [
    "# Network\n",
    "net = ln.models.YoloV2(20)\n",
    "net.load('./yolov2-voc.pt')\n",
    "dimensions = (1, 3, 416, 416)\n",
    "optimizer = torch.optim.SGD(net.parameters(), lr=0.0001)\n",
    "\n",
    "# Pruner\n",
    "pruner = ln.prune.L2Pruner(net, input_dimensions=dimensions, optimizer=optimizer, manner=\"hard\")\n",
    "pruner.prunable_channels"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can now prune our model, and the optimizer will be adapted automatically!  \n",
    "Let us quickly validate this by printing the shape of the first parameter of the network before and after pruning."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "ExecuteTime": {
     "end_time": "2020-12-14T11:27:50.506758Z",
     "start_time": "2020-12-14T11:27:50.413981Z"
    }
   },
   "outputs": [],
   "source": [
    "print(optimizer.param_groups[0]['params'][0].shape)\n",
    "pruned = pruner(0.2)\n",
    "print(optimizer.param_groups[0]['params'][0].shape)\n",
    "pruned"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The second issue we will face when hard pruning, is loading your pruned weights to perform inference.  \n",
    "If we look at our pruned network and compare it to a new YoloV2 instance, we will see that the number of channels in the convolutions do not match."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "ExecuteTime": {
     "end_time": "2020-12-14T11:27:50.783144Z",
     "start_time": "2020-12-14T11:27:50.508155Z"
    }
   },
   "outputs": [],
   "source": [
    "# Original\n",
    "net_original = ln.models.YoloV2(20)\n",
    "net_original"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "ExecuteTime": {
     "end_time": "2020-12-14T11:27:50.790038Z",
     "start_time": "2020-12-14T11:27:50.784809Z"
    }
   },
   "outputs": [],
   "source": [
    "# Pruned\n",
    "net"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Saving the model works just the same, we simply call the [save()](../api/generated/lightnet.network.module.Lightnet.rst#lightnet.network.module.Lightnet.save) function.\n",
    "When loading the network however, we need to tell the model to reduce the number of channels where necessary.\n",
    "The [load_pruned()](../api/generated/lightnet.network.module.Lightnet.rst#lightnet.network.module.Lightnet.load_pruned) method will do this for us automatically!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "ExecuteTime": {
     "end_time": "2020-12-14T11:27:51.621161Z",
     "start_time": "2020-12-14T11:27:50.791781Z"
    }
   },
   "outputs": [],
   "source": [
    "# Save pruned network\n",
    "net.save('yolov2-voc-pruned.pt')\n",
    "\n",
    "# Show difference in weights file size\n",
    "print(os.path.getsize('yolov2-voc.pt'))\n",
    "print(os.path.getsize('yolov2-voc-pruned.pt'))\n",
    "\n",
    "# Load pruned weights\n",
    "net_original.load_pruned('yolov2-voc-pruned.pt')\n",
    "net_original"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "That was it for our pruning tutorial!  \n",
    "You can check out the [Pascal VOC](./03-A-pascal_voc.rst) guide for an example where we train and prune networks on a real dataset!\n",
    "\n",
    "Once you trained and pruned your network, you might want to use this network on a device without Python.  \n",
    "Don't worry, our [Photonnet](https://eavise.gitlab.io/photonnet) C++ library has got your back!\n",
    "\n",
    "<div class=\"alert alert-warning\">\n",
    "\n",
    "**Warning:**\n",
    "\n",
    "Please note that in a real scenario, you will probably want to re-train your network after pruning, in order to retain the same accuracy for your model.\n",
    "For those situations, it is quite important to use a training, validation and test-set.\n",
    "Train and prune your model using the training and validation sets, and finally, test your final model on the test-set to report the accuracy.\n",
    "\n",
    "</div>"
   ]
  }
 ],
 "metadata": {
  "hide_input": false,
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.2"
  },
  "varInspector": {
   "cols": {
    "lenName": 16,
    "lenType": 16,
    "lenVar": 40
   },
   "kernels_config": {
    "python": {
     "delete_cmd_postfix": "",
     "delete_cmd_prefix": "del ",
     "library": "var_list.py",
     "varRefreshCmd": "print(var_dic_list())"
    },
    "r": {
     "delete_cmd_postfix": ") ",
     "delete_cmd_prefix": "rm(",
     "library": "var_list.r",
     "varRefreshCmd": "cat(var_dic_list()) "
    }
   },
   "types_to_exclude": [
    "module",
    "function",
    "builtin_function_or_method",
    "instance",
    "_Feature"
   ],
   "window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
