# Build dependencies
versioneer

# Runtime dependencies
brambox

# Test dependencies
pytest
flake8
flake8-bugbear
flake8-comprehensions
flake8-commas
#flake8-quotes
flake8-simplify
flake8-logging-format

# Docs dependencies
sphinx
sphinx_rtd_theme
recommonmark
nbsphinx
sphinxcontrib-bibtex

-e '.[segment, training]'
