#
#   Test Get*Boxes function on output
#   Note that we only perform some basic sanity checking
#   Copyright EAVISE
#
import pytest
import math
import torch
import lightnet as ln
import lightnet.data.transform as tf


def run_GetBoxes(boxes, tensor, device):
    __tracebackhide__ = True

    if isinstance(tensor, tuple):
        tensor = tuple(t.to(device) for t in tensor)
    else:
        tensor = tensor.to(device)

    out = boxes(tensor)
    assert out.device.type == torch.device(str(device)).type
    return out.cpu()


####################
# Horizontal Boxes #
####################
def check_horizontal_boxes(out, img, cls, conf):
    assert out.ndim == 2
    assert out.shape[1] == 7                                    # Regular anchor box format
    assert set(out[:, 0].unique().tolist()) <= set(range(img))  # Batch_num should be between 0-num_images
    assert (out[:, 5] > conf).all()                             # Confidence should be bigger than threshold
    assert set(out[:, 6].unique().tolist()) <= set(range(cls))  # Class_id should be between 0-num_classes


@pytest.mark.parametrize('image', [1, 8])
@pytest.mark.parametrize('num_classes', [1, 20])
@pytest.mark.parametrize('confidence', [0.1, 0.6])
def test_GetAnchorBoxes(image, num_classes, confidence, device='cpu'):
    anchors = ln.util.Anchors.YoloV2_VOC
    boxes = tf.GetAnchorBoxes(confidence, ln.models.YoloV2.stride, anchors)
    tensor = torch.rand((image, anchors.num_anchors*(num_classes+5), 13, 13))

    out = run_GetBoxes(boxes, tensor, device)
    check_horizontal_boxes(out, image, num_classes, confidence)


@pytest.mark.parametrize('image', [1, 8])
@pytest.mark.parametrize('num_classes', [1, 20])
@pytest.mark.parametrize('confidence', [0.1, 0.6])
def test_GetMultiscaleAnchorBoxes(image, num_classes, confidence, device='cpu'):
    anchors = ln.util.Anchors.YoloV3_COCO
    boxes = tf.GetMultiScaleAnchorBoxes(confidence, ln.models.YoloV3.stride, anchors)
    tensor = (
        torch.rand((image, anchors.get_scale(0).num_anchors*(num_classes+5), 13, 13)),
        torch.rand((image, anchors.get_scale(1).num_anchors*(num_classes+5), 26, 26)),
        torch.rand((image, anchors.get_scale(2).num_anchors*(num_classes+5), 52, 52)),
    )

    out = run_GetBoxes(boxes, tensor, device)
    check_horizontal_boxes(out, image, num_classes, confidence)


@pytest.mark.parametrize('image', [1, 8])
@pytest.mark.parametrize('num_classes', [1, 20])
@pytest.mark.parametrize('confidence', [0.1, 0.6])
def test_GetCornerBoxes(image, num_classes, confidence, device='cpu'):
    boxes = tf.GetCornerBoxes(0.5, confidence, ln.models.Cornernet.stride)
    tensor = torch.rand((image, 2*(num_classes+3), 128, 128))

    out = run_GetBoxes(boxes, tensor, device)
    check_horizontal_boxes(out, image, num_classes, confidence)


##################
# Oriented Boxes #
##################
def check_oriented_boxes(out, img, cls, conf):
    assert out.ndim == 2
    assert out.shape[1] == 8                                    # Oriented anchor box format
    assert set(out[:, 0].unique().tolist()) <= set(range(img))  # Batch_num should be between 0-num_images
    assert (out[:, 5] < math.pi / 4).all()                      # Default max angle
    assert (out[:, 6] > conf).all()                             # Confidence should be bigger than threshold
    assert set(out[:, 7].unique().tolist()) <= set(range(cls))  # Class_id should be between 0-num_classes


@pytest.mark.parametrize('image', [1, 8])
@pytest.mark.parametrize('num_classes', [1, 20])
@pytest.mark.parametrize('confidence', [0.1, 0.6])
def test_GetOrientedAnchorBoxes(image, num_classes, confidence, device='cpu'):
    anchors = ln.util.Anchors.YoloV2_VOC.append_values(0.0)
    boxes = tf.GetOrientedAnchorBoxes(confidence, ln.models.YoloV2.stride, anchors)
    tensor = torch.rand((image, anchors.num_anchors*(num_classes+6), 13, 13))

    out = run_GetBoxes(boxes, tensor, device)
    check_oriented_boxes(out, image, num_classes, confidence)


@pytest.mark.parametrize('image', [1, 8])
@pytest.mark.parametrize('num_classes', [1, 20])
@pytest.mark.parametrize('confidence', [0.1, 0.6])
def test_GetMultiscaleOrientedAnchorBoxes(image, num_classes, confidence, device='cpu'):
    anchors = ln.util.Anchors.YoloV3_COCO.append_values(0.0)
    boxes = tf.GetMultiScaleOrientedAnchorBoxes(confidence, ln.models.YoloV3.stride, anchors)
    tensor = (
        torch.rand((image, anchors.get_scale(0).num_anchors*(num_classes+6), 13, 13)),
        torch.rand((image, anchors.get_scale(1).num_anchors*(num_classes+6), 26, 26)),
        torch.rand((image, anchors.get_scale(2).num_anchors*(num_classes+6), 52, 52)),
    )

    out = run_GetBoxes(boxes, tensor, device)
    check_oriented_boxes(out, image, num_classes, confidence)


################
# Masked Boxes #
################
def check_masked_boxes(out, img, cls, msk, conf):
    assert out.ndim == 2
    assert out.shape[1] == 7+msk                                    # Regular anchor box format with mask coefficients
    assert set(out[:, 0].unique().tolist()) <= set(range(img))      # Batch_num should be between 0-num_images
    assert (out[:, -2] > conf).all()                                # Confidence should be bigger than threshold
    assert set(out[:, -1].unique().tolist()) <= set(range(cls))     # Class_id should be between 0-num_classes


@pytest.mark.parametrize('image', [1, 8])
@pytest.mark.parametrize('num_classes', [1, 20])
@pytest.mark.parametrize('num_masks', [4, 32])
@pytest.mark.parametrize('confidence', [0.1, 0.6])
def test_GetMaskedAnchorBoxes(image, num_classes, num_masks, confidence, device='cpu'):
    anchors = ln.util.Anchors.YoloV2_VOC
    boxes = tf.GetMaskedAnchorBoxes(confidence, num_masks, ln.models.YoloV2.stride, anchors)
    tensor = torch.rand((image, anchors.num_anchors*(num_classes+num_masks+5), 13, 13))

    out = run_GetBoxes(boxes, tensor, device)
    check_masked_boxes(out, image, num_classes, num_masks, confidence)


@pytest.mark.parametrize('image', [1, 8])
@pytest.mark.parametrize('num_classes', [1, 20])
@pytest.mark.parametrize('num_masks', [4, 32])
@pytest.mark.parametrize('confidence', [0.1, 0.6])
def test_GetMultiscaleMaskedAnchorBoxes(image, num_classes, num_masks, confidence, device='cpu'):
    anchors = ln.util.Anchors.YoloV3_COCO
    boxes = tf.GetMultiScaleMaskedAnchorBoxes(confidence, num_masks, ln.models.YoloV3.stride, anchors)
    tensor = (
        torch.rand((image, anchors.get_scale(0).num_anchors*(num_classes+num_masks+5), 13, 13)),
        torch.rand((image, anchors.get_scale(1).num_anchors*(num_classes+num_masks+5), 26, 26)),
        torch.rand((image, anchors.get_scale(2).num_anchors*(num_classes+num_masks+5), 52, 52)),
    )

    out = run_GetBoxes(boxes, tensor, device)
    check_masked_boxes(out, image, num_classes, num_masks, confidence)


##################
# All Boxes CUDA #
##################
@pytest.mark.cuda
@pytest.mark.skipif(not torch.cuda.is_available(), reason='CUDA not available')
@pytest.mark.parametrize('image', [1, 8])
@pytest.mark.parametrize('num_classes', [1, 20])
def test_GetBoxesCUDA(image, num_classes):
    test_GetAnchorBoxes(image, num_classes, 0.4, device='cuda')
    test_GetMultiscaleAnchorBoxes(image, num_classes, 0.4, device='cuda')
    test_GetCornerBoxes(image, num_classes, 0.4, device='cuda')
    test_GetOrientedAnchorBoxes(image, num_classes, 0.4, device='cuda')
    test_GetMultiscaleOrientedAnchorBoxes(image, num_classes, 0.4, device='cuda')
    test_GetMaskedAnchorBoxes(image, num_classes, 8, 0.4, device='cuda')
    test_GetMultiscaleMaskedAnchorBoxes(image, num_classes, 8, 0.4, device='cuda')
