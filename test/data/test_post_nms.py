#
#   Test NMS implementations
#   Copyright EAVISE
#
import pytest
import torch
import pandas as pd
import lightnet.data.transform as tf


@pytest.fixture(scope='module')
def boxes():
    return [
        [0, 125, 125, 250, 250, 0.9, 0],      # Box A
        [0, 225, 125, 250, 250, 0.8, 0],      # Box B : IoU(A,B) > 0.4
        [0, 325, 125, 250, 250, 0.7, 0],      # Box C : IoU(A,C) < 0.4 | IoU(B,C) > 0.4
        [0, 125, 125, 250, 250, 0.6, 1],      # Box D : Other class | IoU(A,D) = 1
        [1, 125, 125, 250, 250, 0.5, 1],      # Box E : Other image | IoU(A,E) = 1
    ]


def test_nms(boxes):
    input_tensor = torch.tensor(boxes)
    input_pd = tf.TensorToBrambox()(input_tensor.clone())
    nms = tf.NMS(0.4)

    # Check tensor output
    out1 = nms(input_tensor)
    assert out1.shape[0] == 4
    assert out1.shape[1] == 7
    assert list(out1[:, 0]) == [0, 0, 0, 1]
    assert list(out1[:, 5]) == [0.9, 0.7, 0.6, 0.5]

    # Compare pandas and torch
    df1 = tf.TensorToBrambox()(out1).sort_values('confidence').reset_index(drop=True)
    df2 = nms(input_pd).sort_values('confidence').reset_index(drop=True)
    pd.testing.assert_frame_equal(df1, df2)


def test_nms_ignore_class(boxes):
    input_tensor = torch.tensor(boxes)
    input_pd = tf.TensorToBrambox()(input_tensor.clone())
    nms = tf.NMS(0.4, class_nms=False)

    # Check tensor output
    out1 = nms(input_tensor)
    assert out1.shape[0] == 3
    assert out1.shape[1] == 7
    assert list(out1[:, 0]) == [0, 0, 1]
    assert list(out1[:, 5]) == [0.9, 0.7, 0.5]

    # Compare pandas and torch
    df1 = tf.TensorToBrambox()(out1).sort_values('confidence').reset_index(drop=True)
    df2 = nms(input_pd).sort_values('confidence').reset_index(drop=True)
    pd.testing.assert_frame_equal(df1, df2)


def test_nms_slow_fast(boxes):
    input_tensor = torch.tensor(boxes)

    out1 = tf.NMS(0.4)(input_tensor)
    out2 = tf.NMS(0.4, memory_limit=0)(input_tensor)

    assert (out1 == out2).all()


def test_nms_soft(boxes):
    input_tensor = torch.tensor(boxes)
    input_pd = tf.TensorToBrambox()(input_tensor.clone())
    nms = tf.NMSSoft(0.4)

    # Check tensor output
    out1 = nms(input_tensor)
    assert out1.shape[0] == 5
    assert out1.shape[1] == 7
    assert list(out1[:, 0]) == [0, 0, 0, 0, 1]
    assert list(out1[:, 1]) == [125, 225, 325, 125, 125]
    # assert list(out1[:, 5]) == []  # TODO : compute scores manually

    # Compare pandas and torch
    df1 = tf.TensorToBrambox()(out1).sort_values('confidence').reset_index(drop=True)
    df2 = nms(input_pd).sort_values('confidence').reset_index(drop=True)
    pd.testing.assert_frame_equal(df1, df2)


def test_nms_soft_fast_ignore_class(boxes):
    input_tensor = torch.tensor(boxes)
    input_pd = tf.TensorToBrambox()(input_tensor.clone())
    nms = tf.NMSSoft(0.4, class_nms=False)

    # Check tensor output
    out1 = nms(input_tensor)
    assert out1.shape[0] == 5
    assert out1.shape[1] == 7
    assert list(out1[:, 0]) == [0, 0, 0, 0, 1]
    assert list(out1[:, 1]) == [125, 225, 325, 125, 125]
    # assert list(out1[:, 5]) == []  # TODO : compute scores manually

    # Compare pandas and torch
    df1 = tf.TensorToBrambox()(out1).sort_values('confidence').reset_index(drop=True)
    df2 = nms(input_pd).sort_values('confidence').reset_index(drop=True)
    pd.testing.assert_frame_equal(df1, df2)


def test_nms_soft_fast(boxes):
    input_tensor = torch.tensor(boxes)
    input_pd = tf.TensorToBrambox()(input_tensor.clone())
    nms = tf.NMSSoftFast(0.4)

    # Check tensor output
    out1 = nms(input_tensor)
    assert out1.shape[0] == 5
    assert out1.shape[1] == 7
    assert list(out1[:, 0]) == [0, 0, 0, 0, 1]
    assert list(out1[:, 1]) == [125, 225, 325, 125, 125]
    # assert list(out1[:, 5]) == []  # TODO : compute scores manually

    # Compare pandas and torch
    df1 = tf.TensorToBrambox()(out1).sort_values('confidence').reset_index(drop=True)
    df2 = nms(input_pd).sort_values('confidence').reset_index(drop=True)
    pd.testing.assert_frame_equal(df1, df2)
